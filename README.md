# 🦀🕸️ Rust et WebAssembly


Projet de démonstration démarré à partir de [`wasm-pack`](https://github.com/rustwasm/wasm-pack).

`Wasm-pack` est un projet développé par le groupe `rust-wasm`, qui ont aussi produit le gabarit npm `create-wasm-app`, les librairies `wasm_bindgen`, le livre de documentation `Rust and WebAssembly` ainsi que d'autres projets permettant de travailler et de s'instruire sur Rust et WebAssembly. 

Présenté par Gabriel Labbé.

# Plan de la démonstration
1. Installation
2. Démonstration du programme en local
3. Programmation web à partir de Rust
4. Compilation du projet en module web
5. Exécution de l'application web


#🛠️ Installation

###Logiciels requis: 
####rustup, rustc et cargo: 
[`Installation de Rust`](https://doc.rust-lang.org/book/ch01-01-installation.html#installation)

####Wasm-pack:
[`Installation de wasm-pack`](https://rustwasm.github.io/wasm-pack/installer/)
```
cargo install wasm-pack
```

####NodeJs (dernière version):
[`Site Web`](https://nodejs.org/)
[`Installation de NodeJs`](https://nodejs.org/en/download/)
######Mise à jour:
```
npm install npm@latest -g
```

####Npx (utilitaire optionnel):
[`npx`](https://www.npmjs.com/package/npx)
```
npm install -g npx
```

####Clôner les fichiers de ce dépôt Git dans un répertoire local:

####`HTTPS`:
```
git clone https://tr3es@bitbucket.org/tr3es/rustwebassembly.git
```
####`SSH`:
```
git clone git@bitbucket.org:tr3es/rustwebassembly.git
```

#🚴 Mise en pratique

## 🐑 Utiliser `Cargo` pour compiler et exécuter localement

Voici la structure du répertoire `fini`:
```
fini
├── Cargo.toml
└── src
    ├── bin.rs
    ├── lib.rs
    └── utils.rs
```

Dans le fichier Cargo.toml, la configuration suivante décrit les cibles d'exécution du projet:
```
[lib]
crate-type = ["cdylib", "rlib"]
name = "lib"

[[bin]]
name = "bin"
path = "src/bin.rs"
```

Cette configuration indique au compilateur de traiter le fichier `src/bin.rs` comme un fichier binaire exécutable et de traiter le fichier `src/lib.rs` en tant que librairie.
Dans ce projet, le fichier `src/bin.rs` ne contient qu'une fonction main qui fait un appel à la fonction `create_random_chien()` dans le fichier `src/lib.rs`.

Cette configuration permet à `Cargo` d'exécuter la fonction `main` du fichier `src/bin.rs` puisqu'il le considère comme étant une cible exécutable.

Donc, à partir de `fini`:
```
cargo build
cargo run
```
La fonction `main` du fichier `src/bin.rs` va s'exécuter et afficher un résultat dans le terminal.

 

## 🐑 Compilation du projet Rust en WebAssembly et exécution sur le Web

#####Le restant de la démo s'effectuera dans le dossier `debut` où nous allons programmer un module web à partir de Rust.
######Si la programmation ne vous intéresse pas, vous pouvez utiliser le dossier `fini` et attendre l'étape de compilation du projet en module web.



## 📚 Programmation Rust dans le fichier lib.rs

À l'aide des librairies [`rand`](https://crates.io/crates/rand) et [`web-sys`](https://crates.io/crates/web-sys), nous allons créer une application web simple.

Cette application va permettre de générer des chiens (comme vu dans l'exécution locale) et d'utiliser du code Rust pour manipuler le [`DOM`](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction) de l'application web.

[`Documentation pour rand`](https://docs.rs/rand/0.6.5/rand/)
[`API de web-sys`](https://rustwasm.github.io/wasm-bindgen/api/web_sys/)

Certaines fonctionalités sont déjà fournies dans le projet. Les signatures des fonctions à programmer sont données dans le fichier `src/lib.rs` et l'initialisation des données est déjà implémenté.


#### 🔬 Attention!
Pour que `rand` soit opérationnel dans le module WebAssembly, la configuration du fichier `Cargo.toml` doit spécifier la version `0.6.5`(minimum) et doit spécifier les fonctionnalités de wasm-bindgen. 

Cette configuration est déjà activée dans les versions fournies du projet.
```
rand = { version = "0.6.5", features = ["wasm-bindgen"] }
```

#### Fonction create_random_chien()
```
pub fn create_random_chien() -> String {
    let _chiens = lecture_chiens().unwrap();
    let _nom_aleatoire = _chiens.dog_names.choose(&mut rand::thread_rng()).unwrap();
    let _race_aleatoire = _chiens.dogs.choose(&mut rand::thread_rng()).unwrap();
    return format!("{} est un {}", _nom_aleatoire, _race_aleatoire);
}
```

#### Fonction generate_chien_to_web()
```
#[wasm_bindgen]
pub fn generate_chien_to_web() {
	set_panic_hook();
    let _window = web_sys::window().expect("no global `window` exists");
    let _document = _window.document().expect("should have a document on window");
    let _bouton = _document.get_element_by_id("wasm_bouton").expect("element with id: wasm_bouton");
    let _val2 = _document.get_element_by_id("texte").expect("Element by Id: texte not found");
    _val2.set_inner_html(&create_random_chien());
}
```


### 🛠️ Initializer un module web avec Node dans le projet Rust:
  
######Installer un module web:
```
npm init
npm install
```


## 🛠️ Compilation du projet en module WebAssembly:

#### À l'aide de l'outil wasm-pack, nous allons construire un module WebAssembly

`Wasm-pack` est un outil qui permet de générer un module `WebAssembly` à partir d'un projet `Rust` ainsi que de permettre à ce module d'interopérer avec `Javascript`.

```
wasm-pack build
```
Cette instruction va générer le dossier pkg/ dans `debut`

#####Résultat:
```
pkg/
├── lib_bg.d.ts
├── lib_bg.wasm
├── lib.d.ts
├── lib.js
└── package.json

```
## 🛠️ Initialization du module Web à l'aide de npm et du gabarit create-wasm-app

[`Github de create-wasm-app`](https://github.com/rustwasm/create-wasm-app)

`Create-wasm-app` est un outil qui permet d'initialiser un module `npm` à partir d'un gabarit simple pour créer une appication web avec `Rust` et `WebAssembly`.

######Initializer un module WebAssembly
```
npm init wasm-app www
```


#### 🛠️ Installation du serveur de développement webpack dans le module NodeJs:
[`Webpack sur Github`](https://github.com/webpack/webpack-dev-server)

`Webpack-dev-server` est un outil qui permet de servir une application `Webpack` en permettant de mettre à jour instantanément l'application si le code source est modifié.
Ce serveur devrait être utilisé uniquement en développement.

###### Installation du serveur dans le dossier `www`
```
npm install webpack-dev-server
```


#### Lier le module WebAssembly au module nodejs:
La commande `npm link` permet de créer des liens symboliques entre plusieurs dossiers de projets web. Dans ce cas-ci, nous allons lier le dossier contenant le code `WebAssembly` et le dossier contenant l'application web.

```
cd pkg
npm link
cd ../www
npm link wasm_demo
```

### 🚴 Modifier le HTML et JS

`index.html`:
```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hello wasm-pack!</title>
  </head>
  <body>
  	<div>
  		<button id="wasm_bouton">Clicker ici pour generer un chien</button>
  	</div>
    <p id="texte"></p>
    <script src="./bootstrap.js"></script>
  </body>
</html>
```
`index.js`:
```
import * as wasm from "wasm_demo";

var clickButton = document.getElementById('wasm_bouton');
clickButton.addEventListener('click', function(){
	wasm.generate_chien_to_web();
});
```

## 🚴 Exécuter l'application web:

À partir du dossier `www`:
```
npm run start
```

#####Résultat:
![Resultat](./resultat.png)
