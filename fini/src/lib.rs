mod utils;
use utils::*;
use wasm_bindgen::prelude::wasm_bindgen;
use serde::Deserialize;
use serde::Serialize;
use std::error::Error;
use rand::prelude::SliceRandom;

//Structure permettant de contenir les donnees lues depuis la méthode utils::get_data_as_string().
#[derive(Serialize, Deserialize)]
struct Chiens {
	dog_names: Vec<String>,
	dogs: Vec<String>
}

//Fonction permettant d'insérer les données lues depuis utils::get_data_as_string() dans la structure Chiens.
fn lecture_chiens() -> Result<Chiens, Box<Error>> {
	let dogs = serde_json::from_str(&get_data_as_string())
		.expect("Impossible de lire la string en json");
	Ok(dogs)
}

//Fonction qui choisit aléatoirement un nom et une race de chien, et retourne une String formatée annonçant quel est le chien créé.
pub fn create_random_chien() -> String {
    let _chiens = lecture_chiens().unwrap();
    let _nom_aleatoire = _chiens.dog_names.choose(&mut rand::thread_rng()).unwrap();
    let _race_aleatoire = _chiens.dogs.choose(&mut rand::thread_rng()).unwrap();
    return format!("{} est un {}", _nom_aleatoire, _race_aleatoire);
}

//Fonction permettant d'afficher un chien dans la page web.
#[wasm_bindgen]
pub fn generate_chien_to_web() {
	set_panic_hook();
    let _window = web_sys::window().expect("no global `window` exists");
    let _document = _window.document().expect("should have a document on window");
    let _bouton = _document.get_element_by_id("wasm_bouton").expect("element with id: wasm_bouton");
    let _val2 = _document.get_element_by_id("texte").expect("Element by Id: texte not found");
    _val2.set_inner_html(&create_random_chien());
}